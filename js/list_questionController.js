document.addEventListener("deviceready", onDeviceReady, false);

var b = 0;
var test = {};
var pitanja = {};
var limit = 0;
var svaPitanja = [];
var correct = [];

class Odgovor {
  constructor(id, temp) {
    this.id = id;
    this.temp = temp;
  }
}

//izvlacenje trenutnog testa iz localStorage-a i ispisivanje pitanja
function onDeviceReady(){
    document.addEventListener("pause", onPause, false);
    test = JSON.parse(localStorage.trenutniTest);
    getLParts(test.id);
}

function onPause() {
    window.location.pathname = "/android_asset/www/list_show_tests.html";
}

function getLParts(collection_id){
    //vadjenje partova sa servera preko ajax req
    var req = new XMLHttpRequest();

    req.onreadystatechange = function(){
        if (this.readyState == 4 && this.status == 200)
        {
            potrebniPartovi = JSON.parse(this.responseText);
            //u slucaju da nema takvih partova
            if (potrebniPartovi.length == 0){
                console.log("No parts found!");
            }else{
                writeLParts();
            }
        }else{
            console.log(this);
        }
    };
    //ovde ce trebati da se menja url
    req.open(
            "GET",
            "https://hippo-tests-2018.000webhostapp.com/php_functions/listening/getTestsForCollection.php?id=" + collection_id,
            true
            );
     req.send();
}

function writeLParts(){
    for (var k = 0; k <= potrebniPartovi.length - 1; k++) {
        part_holder = document.createElement('div');
        var wrap = document.createElement('div');
        wrap.setAttribute('id', 'wrapQuestion'+ potrebniPartovi[k].id);
        wrap.classList.add('fade-in');
        part_holder.setAttribute('id', 'PartHolder' + potrebniPartovi[k].id);
        part_holder.classList.add('fade-in');
        var part_name = document.createElement('h5');
        part_name.classList.add('fade-in');
        part_name.classList.add('part-name');
        var part_reading_text = document.createElement('p');
        part_reading_text.classList.add('reading-text');
        var reading_text_holder = document.createElement('div');
        reading_text_holder.setAttribute('style', 'display: block;');
        reading_text_holder.appendChild(part_reading_text);
        wrap.appendChild(part_name);
        wrap.appendChild(reading_text_holder);
        part_name.innerText = potrebniPartovi[k].name.substring(2);
        showImage(potrebniPartovi[k], wrap);
        if(image != null){
            wrap.appendChild(image);
        }
        part_reading_text.innerHTML = potrebniPartovi[k].reading_text;
        getLQuestions(potrebniPartovi[k].id);
        part_holder.classList.add('part-holder-div');
        part_holder.appendChild(wrap);
        document.body.appendChild(part_holder);
    }
    writeTest();
    var submitDiv = document.createElement('div');
    submitDiv.setAttribute("class", "col-md-6");
    submitDiv.classList.add('fade-in');
    var submitButton = document.createElement('a');
    submitButton.setAttribute("class", "btn btn-success submit-width fade-in-submit");
    submitButton.setAttribute("href", "result.html");
    submitButton.setAttribute("ontouchstart", "submitAnswers()");
    submitButton.classList.add('fade-in');
    submitButton.innerText = "Submit";
    submitDiv.appendChild(submitButton);
    document.body.appendChild(submitDiv);
    document.body.appendChild(document.createElement("br"))
}

function getLQuestions(id)
{
    var req = new XMLHttpRequest();

    req.onreadystatechange = function()
    {
        if (this.readyState == 4 && this.status == 200)
        {
            pitanja = JSON.parse(this.responseText);
            writeQuestions(id);
        }
    };
    req.open(
            "GET",
            "https://hippo-tests-2018.000webhostapp.com/php_functions/listening/getQuestions.php?test_id=" + id,
            true
            );
     req.send();
}


//skupljanje odgovora u array koji ce biti iste duzine kao odg
function submitAnswers()
{
    submitedAnswers = [];
    var counter = 0;
    var qLimit = svaPitanja.length;
    //console.log(svaPitanja[0].id);
    for(var i = 0; i < qLimit; i++)
    {
        var temp = document.querySelector('input[name="question' + svaPitanja[i].id + '"]:checked');
        if(temp != null)
        {
            //nzm da li da uopste cuvamo date odg, nzm jel nam trebaju kasnije?
            let submAns = new Odgovor(svaPitanja[i].id, temp.value);
            submitedAnswers.push(submAns);
            if(submitedAnswers[i].temp == svaPitanja[i].correct_answer)
            {
                counter++;
            }
        }
        else
        {
            var id = cutStr(svaPitanja[i].question);
            let nullObj = new Odgovor(id, "null");
            submitedAnswers.push(nullObj);
        }
    }
    localStorage.setItem("subAns", JSON.stringify(submitedAnswers));
    localStorage.correctAnswers = counter;
    localStorage.numOfAnswers = qLimit;
    localStorage.setItem("tac_odg", JSON.stringify(correct));

}

//ispisujemo reading_text i ime(ovde sigurno treba poraditi na izgledu)
function writeTest()
{
    var name = document.getElementById('test_name_text');
    name.classList.add('fade-in');
    name.classList.add('test-text');
    name.innerText = test.name;
    var play = document.getElementById('play');
    //play.classList.add('fade-in');
    play.setAttribute('onclick', 'playAudio(' + test.id + ');');
    play.setAttribute('class', 'fade-in-submit btn btn-success');
}

//ispisivanje svih pitanja i odg
function writeQuestions(id)
{
    var wrap = document.getElementById('wrapQuestion' + id);
    var holder = document.getElementById('PartHolder' + id);
    limit = pitanja.length;
    for(var k = 0; k < limit; k++)
    {
        //za svako pitanje stvaramo novu formu u koju ubacujemo text pitanja
        svaPitanja.push(pitanja[k]);
        var id = cutStr(pitanja[k].question);
        var tacan = new Odgovor(id, pitanja[k].correct_answer);
        correct.push(tacan);
        var form = document.createElement('form');
        form.setAttribute('id', 'multipleChoiceForm');
        form.setAttribute('class', 'forms');
        form.classList.add('fade-in');
        var div = document.createElement('div');
        div.classList.add('question-text');
        div.classList.add('fade-in');
        div.innerText = pitanja[k].question;
        form.appendChild(div);
        //stavljamo sve odgovore ovih pitanja u formu, dajemo im isto ime
        //da bi mogli da izvucemo vrednost preko querySelectora i da bi ogranicili na samo jedan selectovan radio button
        var odgovori = [];
        //ubacujemo odgovore za jedno pitanje u array, jer posle hocu da dodam random redosled odgovora
        if(pitanja[k].incorrect_answer2 == ""){
            odgovori.push(pitanja[k].correct_answer, pitanja[k].incorrect_answer1);
            b = 2;
        }
        else if (pitanja[k].incorrect_answer3 == "")
        {
            odgovori.push(pitanja[k].correct_answer, pitanja[k].incorrect_answer1, pitanja[k].incorrect_answer2);
            b = 3;
        }
        else
        {
            odgovori.push(pitanja[k].correct_answer, pitanja[k].incorrect_answer1, pitanja[k].incorrect_answer2, pitanja[k].incorrect_answer3);
            b = 4;
        }
        odgovori = shuffle(odgovori);
        for(var i = 0; i < b; i++)
        {
            var label = document.createElement('label');
            label.setAttribute('class', 'radio');
            var input = document.createElement('input');
            var span = document.createElement('span');
            span.setAttribute('style', 'display: block; padding-top: 2.5px;');
            var br = document.createElement('br');
            input.setAttribute('type', 'radio');
            input.setAttribute('name' , 'question' + pitanja[k].id);
            input.setAttribute('value' , odgovori[i]);
            span.innerText = odgovori[i];
            label.appendChild(input);
            label.appendChild(span);
            form.appendChild(label);
            form.appendChild(br);
        }
        wrap.appendChild(form);
    }
    // holder.classList.remove('hidden');
    // holder.classList.add('fade-in');
}

function playAudio(id)
{
    var a = new Audio('https://hippo-tests-2018.000webhostapp.com/audio/audio' + id + '.mp3');
    a.play();
    var play = document.getElementById('play');
    play.parentNode.removeChild(play);
}
function showImage(part, wrap)
{
    if(part.image_file != ""){
        var image = document.createElement('img');
        image.setAttribute('src', 'https://hippo-tests-2018.000webhostapp.com/images/image' + part.id + '.jpg');
        image.setAttribute('width', '80%');
        image.setAttribute('height', '35%');
        image.setAttribute('class', 'slika');
        image.classList.add('fade-in');
        wrap.appendChild(image);
    }
}

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

function cutStr(string){
    var cut;
    if (string[1] == ".") {
        cut = string.substring(0,1);
    }     
    else{
        cut = string.substring(0,2);
    }
    var broj = parseInt(cut);
    return broj;
}