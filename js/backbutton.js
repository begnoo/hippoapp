document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady(){
    document.addEventListener("backbutton", onBackButton, false);
}

function onBackButton(){
    var location = window.location.pathname;
    if(location == "/android_asset/www/start.html"){
        navigator.app.exitApp();
    }
    else if(location == "/android_asset/www/result.html"){
        window.location.pathname = "/android_asset/www/start.html";
    }
    else if(location == "/android_asset/www/credits.html"){
        navigator.app.exitApp();
    }
}