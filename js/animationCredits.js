document.addEventListener("deviceready", onDeviceReady, false);

var op = 0;

function onDeviceReady(){
    startAnimation();
}

setTimeout(endAnimation, 2500);
setTimeout(loadPage, 5000);

function startAnimation(){  
    var elem = document.getElementById("credits"); 

    var id = setInterval(frame, 10);
    function frame() {
        if (op == 1) {
            clearInterval(id);
        } else {
            op += 0.005; 
            elem.style.opacity = op; 
        }
    }
}

function endAnimation(){
    var elem = document.getElementById("credits"); 

    var interv = setInterval(frame, 10);
    function frame() {
        if (op == 0) {
            clearInterval(interv);
        } else {
            op -= 0.01; 
            elem.style.opacity = op; 
        }
    }
}

function loadPage(){
    window.location.href = 'start.html';
}