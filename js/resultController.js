document.addEventListener("deviceready", onDeviceReady, false);


var correctAnswers = localStorage.correctAnswers;
var numOfAnswers = localStorage.numOfAnswers;

var submitedAnswers = JSON.parse(localStorage.getItem("subAns"));
var tacni_odgovori = JSON.parse(localStorage.getItem("tac_odg"));

function onDeviceReady()
{
    showRes();
    showAnswers();
    
    console.log(tacni_odgovori);
}

function showRes()
{
   var result = document.createElement('div');
   result.setAttribute('class', 'res-text');
   result.innerHTML = "You scored: " + correctAnswers + "/" + numOfAnswers + "<br>" + perTest(correctAnswers, numOfAnswers) + "%";
   var div = document.getElementById("results");
   div.appendChild(result);
}

function showAnswers(){
	
	var wrap = document.getElementById('answers');
	var list = document.getElementById('list');

	tacni_odgovori.sort(function (a, b) {return a.id - b.id;});
	submitedAnswers.sort(function (a, b) {return a.id - b.id;});

	for (var i = 0; i <= submitedAnswers.length - 1; i++) {

		var ans = document.createElement('li');
		
		if(submitedAnswers[i].temp != "null") {
			if (submitedAnswers[i].temp == tacni_odgovori[i].temp) {
				//ispisuje tacan odgovor
				ans.innerHTML = tacni_odgovori[i].temp;
				ans.setAttribute('style', 'color: green;');
			}
			else {
				//ispisuje netacan odgovor 
				//PS: ne znam da li da stavim else if(pogresni_odgovori ..)
				ans.innerHTML = submitedAnswers[i].temp;
				ans.setAttribute('style', 'color: red;');
			}
		}
		else{
			ans.innerHTML = "No answer given.";
			ans.setAttribute('style', 'color: grey;');
		}
		list.appendChild(ans);
	}
	wrap.appendChild(list);
}

function perTest(correct, all){
    return Math.round((correct/all)*100);
}


